FROM caddy:2-alpine

WORKDIR /srv

COPY public/ . 

ENTRYPOINT [ "caddy" ]
CMD [ "file-server", "--root", "/srv", "--browse", "--access-log" ]


